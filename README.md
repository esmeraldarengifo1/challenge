# Challenge

<img src="https://img.shields.io/badge/php-%5E7.*-blue"/>

<!-- TO DO: DESCRIPTION -->

Php challenge where we print a matrix with the numbers from 1 to 100, for multiples of 3 "Falabella" is printed, multiples of 5 "IT" and finally for multiples of 3 and 5 "Integrations".

## Contribution

<!-- TO DO: Contribution -->

- Modify the file in `Challenge.php`
- Add more files

## Authors

* Yoselyn Ramirez [@esmeraldarengifo1](https://gitlab.com/esmeraldarengifo1)

